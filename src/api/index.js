import axios from 'axios';

export const fetchProductsApi = ({ keyword, price, subscription }) => {
  const url = new URL('http://localhost:3010/products');

  appendQueryParams('tags_like', keyword, url);
  appendQueryParams('price', price, url);
  appendQueryParams('subscription', subscription, url);

  return axios.get(url.toString());
};

const appendQueryParams = (paramsName, paramsValue, url) => {
  if (paramsValue) url.searchParams.set(paramsName, paramsValue);
};
