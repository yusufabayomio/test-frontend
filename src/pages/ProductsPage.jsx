import SideBar from '../components/sidebar/sidebar';
import Products from '../components/products/Products';

const ProductsPage = () => {
  return (
    <div className='container mt-5'>
      <div className='row'>
        <SideBar />
        <Products />
      </div>
    </div>
  );
};

export default ProductsPage;
