import { useState } from 'react';
import PropTypes from 'prop-types';
import { getQueryParams } from '../../utils/helpers';

const propTypes = {
  setUrlParams: PropTypes.func.isRequired,
};

const SubscriptionFilter = ({ setUrlParams }) => {
  const [checked, setChecked] = useState(getQueryParams()['subscription'] === 'true' ? true : false);

  const onChangeHandler = () => {
    setChecked(!checked);
    setUrlParams(!checked);
  };

  return (
    <label className='d-flex justify-content-between'>
      <p>Subscription</p> <input type='checkbox' checked={checked} onChange={onChangeHandler} />
    </label>
  );
};

SubscriptionFilter.propTypes = propTypes;

export default SubscriptionFilter;
