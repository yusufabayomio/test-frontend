import { useEffect, useState } from 'react';
import qs from 'query-string';
import FilterForm from '../form-filter/FilterForm';
import SubscriptionFilter from '../subscription-filter/SubscriptionFilter';
import history from '../../utils/history';
import { getQueryParams } from '../../utils/helpers';
import { useDispatch } from 'react-redux';
import { fetchProducts } from '../../states/actions';

const SideBar = () => {
  const [keyword, setKeyword] = useState(getQueryParams()['keyword']);
  const [price, setPrice] = useState(getQueryParams()['price']);
  const [subscription, setSubscription] = useState(getQueryParams()['subscription']);
  const dispatch = useDispatch();

  const updateUrlParams = (key, value) => {
    let params = getQueryParams();
    if (value) {
      params = {
        ...params,
        [key]: value,
      };
    } else {
      delete params[key];
    }
    history.push(`?${qs.stringify(params)}`);
  };

  const onFilterByKeywordHandler = (keyword) => {
    setKeyword(keyword);
    updateUrlParams('keyword', keyword);
  };

  const onFilterByPriceHandler = (price) => {
    setPrice(price);
    updateUrlParams('price', price);
  };

  const onFilterBySubscription = (subscription) => {
    const subscriptionParams = subscription ? 'true' : 'false';
    setSubscription(subscriptionParams);
    updateUrlParams('subscription', subscriptionParams);
  };

  useEffect(() => {
    dispatch(fetchProducts({ keyword, price, subscription }));
  }, [keyword, price, subscription, dispatch]);

  return (
    <div className='col-md-3'>
      <h6>Filter Products</h6>
      <FilterForm inputType='text' inputKey='keyword' placeholder='Filter by product name' setUrlParams={onFilterByKeywordHandler} />
      <FilterForm inputType='number' inputKey='price' placeholder='Filter by price' setUrlParams={onFilterByPriceHandler} />
      <SubscriptionFilter setUrlParams={onFilterBySubscription} />
    </div>
  );
};

export default SideBar;
