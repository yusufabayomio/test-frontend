import { useSelector } from 'react-redux';
import Product from '../product/Product';

const Products = () => {
  const { error, loading, products } = useSelector((reducer) => reducer);
  return (
    <div className='col-md-9'>
      {loading && !products && <p className='text-center'>Fetching Products ...</p>}
      {products && !products.length && <p className='text-center'>No Result Found</p>}
      {error && <p className='text-center'>Error fetching products</p>}
      {products && products.length > 0 && (
        <div className='row'>
          <h6>{products.length} product(s) found</h6>
          {products.map((product) => (
            <Product key={product.id} product={product} />
          ))}
        </div>
      )}
    </div>
  );
};

export default Products;
