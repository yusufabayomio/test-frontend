const Product = ({ product }) => {
  return (
    <div className='col-md-4 mb-4'>
      <div className='card'>
        <img src={`${product.image_src}`} alt={`${product.title}`} />
        <div className='card-body'>
          <h6>
            <a href={product.url} target='blank'>
              {product.title}
            </a>
          </h6>
          <div className='d-flex justify-content-between mb-3'>
            <span>£{product.price}</span> {product.subscription && <span className='badge bg-danger badge-sm me-1'>{product.subscription_discount}% discount</span>}
          </div>
          {product.tags.map((tag) => (
            <span className='badge bg-success me-1' key={tag}>
              {tag}
            </span>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Product;
