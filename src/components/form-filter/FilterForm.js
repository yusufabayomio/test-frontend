import PropTypes from 'prop-types';
import { useState } from 'react';
import { getQueryParams } from '../../utils/helpers';
import './FilterForm.css';

const propTypes = {
  inputType: PropTypes.string.isRequired,
  inputKey: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  setUrlParams: PropTypes.func.isRequired,
};

const FilterForm = ({ inputType = 'text', inputKey, placeholder, setUrlParams }) => {
  const [value, setValue] = useState(getQueryParams()[inputKey] ?? '');

  const onChangeHandler = (e) => {
    setValue(e.target.value);
    if (!e.target.value) {
      setUrlParams('');
    }
  };

  const onSubitHandler = (e) => {
    e.preventDefault();
    setUrlParams(value);
  };

  return (
    <form onSubmit={onSubitHandler}>
      <div className='form-group mb-3'>
        <input type={inputType} value={value} onChange={onChangeHandler} className='form-control' placeholder={placeholder} />
      </div>
    </form>
  );
};

FilterForm.propTypes = propTypes;

export default FilterForm;
