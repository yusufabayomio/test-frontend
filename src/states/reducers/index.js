import { FETCHING_PRODUCTS, FETCH_PRODUCT_ERROR, FETCH_PRODUCT_SUCCESS } from '../action-types';

const initialState = {
  loading: false,
  products: null,
  error: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_PRODUCTS:
      return { ...state, loading: true, error: false };
    case FETCH_PRODUCT_SUCCESS:
      return { ...state, loading: false, products: action.payload };
    case FETCH_PRODUCT_ERROR:
      return { ...state, loading: false, error: true };
    default:
      return { ...state };
  }
};

export default reducer;
