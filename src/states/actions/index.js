import { fetchProductsApi } from '../../api';
import { FETCHING_PRODUCTS, FETCH_PRODUCT_ERROR, FETCH_PRODUCT_SUCCESS } from '../action-types';

export const fetchProducts =
  ({ keyword, price, subscription }) =>
  async (dispatch) => {
    try {
      dispatch({
        type: FETCHING_PRODUCTS,
      });

      const response = await fetchProductsApi({ keyword, price, subscription });
      dispatch({
        type: FETCH_PRODUCT_SUCCESS,
        payload: response.data,
      });
    } catch (error) {
      // ideally log error to an error log service
      console.log(error);
      dispatch({
        type: FETCH_PRODUCT_ERROR,
      });
    }
  };
