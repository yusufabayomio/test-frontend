import qs from 'query-string';

export const getQueryParams = () => {
  const search = window.location.search;
  return qs.parse(search);
};
