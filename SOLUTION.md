# SOLUTION

## Estimation

Estimated: 3 hours

Spent: 4 hours

## Solution

Comments on your solution

My solution relies heavily on modifying the browser history. I followed this pattern for the following reasons:

1. Enchance SEO
2. User convinience: a user can refresh their browser and still has all the search parameter in place. This makes it possible to bookmark, share and save a descired filter criteria set.
3. Refer back to the previous filter by using the browser history.

### Solution Behavior

```gherkin
WHEN I visit the product collection page
THEN  I expect to see filters sidebar
AND I expect to see a list of products
AND I expect to see "11" products on the product list
AND I expect to see the total number of products

WHEN I visit the product collection page
THEN  I expect to see filters sidebar
AND I search for "Dog" in the filters sidebar
THEN my browser url should update with the query parameter "?keyword=Dog"
AND I expect to see "10" products on the product list

WHEN I visit the product collection page
THEN  I expect to see filters sidebar
AND I filter by "Price" "127" in the filters sidebar
THEN my browser url should update with the query parameter "?price=127"
AND I expect to see "4" products on the product list

WHEN I visit the product collection page
THEN  I expect to see filters sidebar
AND I filter by checking the subscription checkbox
THEN my browser url should update with the query parameter "?subscription=true"
AND I expect to see "7" products on the product list
```

### Product Improvements

1. Improve the UI of the application
2. Add a search icon when the text input state is empty and a 'cancel search' icon when the text input state is not empty
3. Add a "clear entire filter" feature
4. Communicate with the backend team to improve the API to return total number of records so as to make pagination possible on the frontend
5. Lazy load the product images, so the page can have a faster load time (in a real-word scenerio where there are thousands of requests)

Estimated time to implement the improvement is 3 to 5 hours
